from pickle import load, dump




domains_per_protein=load(open("graph data/domains_per_protein.p"))
print "Training Proteins Loaded-----\n"
proteins_per_domain=load(open("graph data/proteins_per_domain.p"))
print "Training Domains are loaded\n"


def find_neighbors(domains ):

    neigbors=set()
    for dom in domains:

        if proteins_per_domain.has_key(dom):

            #P= [x[0] for x in proteins_per_domain[dom]]
            P=proteins_per_domain[dom]

            neigbors=set.union(neigbors,set(P))
    #print neigbors
    return neigbors

def link_strength(D1, D2):
    D1=set(D1)
    D2=set(D2)
    match=len(set.intersection( D1, D2))
    union=len(set.union(D1,D2))
    return float(match)/float(union)
def  create_graph(th=0.01):
	w=open("SwissProt_EC_Grapah.txt", 'wb')
	edges=set()
	for protein in domains_per_protein:
		#nodes.add(protein)
		doms=domains_per_protein[protein]
		NN=find_neighbors(doms)
		for n in NN:
			if n==protein:
				continue
			if (protein, n) in edges:
				continue
			if (n, protein) in edges:
				continue
			d2=domains_per_protein[n]	
			wij=link_strength(doms, d2)
			wij=round(wij, 3)

			if wij>th:
				w.write(protein+"\t"+n+"\t"+str(wij)+"\n")
			edges.add((protein,n))
			print "Edge Added", protein, "<-->", n, wij 

	w.close()



if __name__ == '__main__':
	create_graph(0.001)




