

import matplotlib.pyplot as plt
import sys


#==================================================
#
#Comparing GO terms
#==================================================

def precision_go(A,P):
    M=len(set.intersection(set(A), set(P)))
    N=len(set(P))
    return float(M)/float(N)

def recall_go(A,P):
    M=len(set.intersection(set(A), set(P)))
    N=len(set(A))
    return float(M)/float(N)

def score_prediction_go(A,P):
    return precision_go(A,P), recall_go(A,P)
def f_measure_go(pre, rec):
    return 2*(pre*rec)/(pre+rec)
#=========================================================
# Macro Precision, Recall, and F-Measure
#
#========================================================

def macro_precision(truth, pred):
    tp=set.intersection(set(truth), set(pred))
    l1=len(tp)
    L=len(set(pred))
    return float(l1)/float(L)

def macro_recall(truth, pred):
    tp = set.intersection(set(truth), set(pred))
    l1 = len(tp)
    L = len(set(truth))
    return float(l1) / float(L)
def macro_f(pre, rec):
    return 2.0*pre*rec/(pre+rec)


def get_predictions_per_labels(labels):
    classes={}
    for p in labels:

        ecs=labels[p]
        s=set()
        for ec in ecs:
            t=ec.split('.')[0]
            s.add(int(t))
        for ss in list(s):
            if classes.has_key((ss)):
                classes[ss].append(p)
            else:
                classes.update({ss:[p]})
    return classes

def prediction_analysis(pred_file, truth_file):
    tr={}
    pr={}

    T=read_truthfile(truth_file)
    P=read_predictionfile(pred_file)

    print"======================================\n"
    tr=get_predictions_per_labels(T)
    pr=get_predictions_per_labels(P)
    pre=0.0
    rec=0.0
    f=0.0
    for l in tr:
        t=tr[l]
        print t
        p=pr[l]
        x=(macro_precision(t,p), macro_recall(t, p))
        fx=macro_f(x[0], x[1])
        print l, x, fx
        pre=pre+x[0]
        rec=rec+x[1]
        f=f+fx

    print pre/len(tr), rec/len(tr), f/len(tr)

def confusion_matrix(pred_file, truth_file):
    T = read_truthfile(truth_file)
    P = read_predictionfile(pred_file)


#============================================================================
# Measuring EC number similarity
#============================================================================
def compare_ec_number(ec1, ec2):
    ec1 = ec1.strip().split('.')
    #print ec1
    ec2 = ec2.strip().split('.')
    #print ec2
    score = 0.0
    for i in range(4):

        if ec1[i] == ec2[i] or ec2[i] == '-':
            score = score + 1
        else:
            break
    return score

def compare_ec_number_2(ec1, ec2, kmatch=1):
    ec1 = ec1.strip().split('.')
    #print ec1
    ec2 = ec2.strip().split('.')
    #print ec2
    score = 0.0
    
    if kmatch==1:
        if ec1[0] == ec2[0]:# or ec2[i] == '-':
            return True 
        else:
            return False
    elif kmatch==2:
        if (ec1[0] == ec2[0]) and (ec1[1]==ec2[1]):# or ec2[i] == '-':
            return True 
        else:
            return False
    elif kmatch==3:
        if (ec1[0] == ec2[0]) and (ec1[1]==ec2[1]) and (ec1[2]==ec2[2]) :# or ec2[i] == '-':
            return True 
        else:
            return False
    elif kmatch==4:
        if (ec1[0] == ec2[0]) and (ec1[1]==ec2[1]) and (ec1[2]==ec2[2]) and (ec1[3]==ec2[3]) :# or ec2[i] == '-':
            return True 
        else:
            return False

def compute_correct_prediction(A, P, k_match):
    match = set()
    for p in P:
        for a in A:
            if compare_ec_number_2(p, a, k_match): #>= k_match:
                match.add(p)
                break
    return match


def score_prediction(A, P, k_match):
    T=set(A)
    #for a in A:
    #    T.add(a)
    F=set(P)
    #for p in P:
    #    F.add(p)

    accuracy = 0.0

    match=compute_correct_prediction(T, F, k_match)
    accuracy=float(len(match))

    precision= round(accuracy / len(F),2)
    
    #if accuracy<len(T):
    #    recall=round(accuracy / len(T),2)
    #else:
    #    recall=1.00
    recall=precision
    return (precision, recall)# precision and recall respectively

def average_score(scores):
    precision=0.0
    recall=0.0
    l=float(len(scores))
    for sc in scores:
        p, r=scores[sc]
        precision=precision+p
        recall=recall+r
    pre=round(precision/l,2)
    re=round(recall/l,2)
    F1 = (2.0 * pre * re) / (pre + re)
    return pre , re,F1
#==========================end================================
#
# Read output files
#
#=============================================================

def read_truthfile(filename):
    R={}
    with open(filename) as F:
        for line in F:
            #print line 
            ac, ec=line.strip().split()[:2]

            if R.has_key(ac):
                R[ac].append(ec)
            else:
                R.update({ac:[ec]})
    return R


def read_predictionfile(filename):
    R = {}
    with open(filename) as F:
        for line in F:
            ac, ec = line.strip().split()[:2]
            if R.has_key(ac):
                R[ac].append(ec)
            else:
                R.update({ac: [ec]})
    return R

#====================================================
# Evaluate the total prediction score
#
#===================================================




def evaluate(pred_file, truth_file, ec_level=1):
    e={}
    T=read_truthfile(truth_file)
    P=read_predictionfile(pred_file)

   
    for i in T:
        if P.has_key(i):
            t=T[i]
            #p= [x[0] for x in P[i]]
            #CF.write(str(t)+'\t'+str(P[i])+'\n')
            p=P[i]
            #print i, t, p 
            
            sc=score_prediction(t, p, ec_level)
            #print "Truth:",t, "Prediction:",p, "Score:",sc
            e.update({i:sc})
        
    Total_prediction=len(e)
    c=float(Total_prediction)/len(T)
    p,r,f=average_score(e)
    return len(T),Total_prediction,c,p 

    


if __name__ == '__main__':
    print "Please ente the prediction file first and then Ground truth"
    f1=sys.argv[1]
    f2=sys.argv[2]
    T,TP,C,E=evaluate(f1,f2,1)
    print 'Total Proteins', T
    print "Total Predicted", TP
    print 'Coverage', C
    print "Accuracy",E
