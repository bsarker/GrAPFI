This is the 1244 E coli proteins for training of

Chengxin Zhang, Wei Zheng, Peter L. Freddolino, and Yang Zhang. 
MetaGO: A Structure and Protein-Protein Interaction Network Based Pipeline
for Automated Protein Function Annotations. J. Mol. Biol.

All proteins are referred by their UniProt accession.
You can find the following files in this folder:
[1] seq.txt - fasta sequence for all targets.
[2] ECOLI_GOterms.CAFA.{MF,BP,CC}.is_a - experimental GO annotation for all
    targets (evidence code EXP, IDA, IMP, IGI, IEP, TAS or IC).
[3] ITASSER_model.tar.bz2 - I-TASSER predicted structure models for all
    targets, with templates sharing >=30% sequence identity to target
    removed.
